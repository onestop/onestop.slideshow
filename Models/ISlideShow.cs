﻿using System.Collections.Generic;

namespace Onestop.SlideShows.Models {
    public interface ISlideShow {
        IEnumerable<int> SlideIds { get; set; }
        IEnumerable<SlidePart> Slides { get; }

        string Plugin { get; set; }
        string Get(string settingName, string defaultValue = null);
        ISlideShow Set(string settingName, string value);
    }
}