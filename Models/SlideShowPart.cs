﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Utilities;
using Orchard.Environment.Extensions;

namespace Onestop.SlideShows.Models {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlideShowPart : ContentPart, ISlideShow {
        private IDictionary<string, string> _pluginConfiguration;
        private IDictionary<string, string> _defaultConfiguration;
        internal readonly LazyField<IEnumerable<SlidePart>> LazySlides = new LazyField<IEnumerable<SlidePart>>();

        public IEnumerable<int> SlideIds {
            get { return DecodeIds(this.Retrieve<string>("SlideIds", versioned: true)); }
            set { this.Store("SlideIds", EncodeIds(value), versioned: true); }
        }

        public IEnumerable<SlidePart> Slides {
            get { return LazySlides.Value; }
        }

        public string Plugin {
            get { return this.Retrieve(x => x.Plugin, versioned: true); }
            set { this.Store(x => x.Plugin, value, versioned: true); }
        }

        public string PluginConfiguration {
            get { return this.Retrieve(x => x.PluginConfiguration, versioned: true); }
            set {
                this.Store(x => x.PluginConfiguration, value, versioned: true);
                _pluginConfiguration = null;
            }
        }

        public string Get(string settingName, string defaultValue = null) {
            var config = EnsurePluginConfiguration();
            string setting;
            config.TryGetValue(settingName, out setting);

            if (!String.IsNullOrWhiteSpace(setting))
                return setting;

            if (_defaultConfiguration != null && _defaultConfiguration.TryGetValue(settingName, out setting)) {
                return setting ?? defaultValue;
            }
            return defaultValue;
        }

        public ISlideShow Set(string settingName, string value) {
            var config = EnsurePluginConfiguration();
            config[settingName] = value;
            this.Store("PluginConfiguration", JsonConvert.SerializeObject(config), versioned: true);
            return this;
        }

        public void SetConfiguration(Dictionary<string, string> pluginConfiguration) {
            _pluginConfiguration = pluginConfiguration;
        }

        public void SetFallbackConfiguration(IDictionary<string, string> defaultConfig) {
            _defaultConfiguration = defaultConfig;
        }

        private IDictionary<string, string> EnsurePluginConfiguration() {
            var pluginConfigurationData = this.Retrieve<string>("PluginConfiguration", versioned: true);
            return _pluginConfiguration ??
                   (_pluginConfiguration =
                   String.IsNullOrWhiteSpace(pluginConfigurationData)
                   ? new Dictionary<string, string>()
                   : JsonConvert.DeserializeObject<Dictionary<string, string>>(pluginConfigurationData));
        }

        private static string EncodeIds(IEnumerable<int> ids) {
            var list = ids.ToList();
            if (ids == null || !list.Any()) {
                return string.Empty;
            }

            return String.Join(",", list);
        }

        private static IEnumerable<int> DecodeIds(string ids) {
            if (String.IsNullOrWhiteSpace(ids)) {
                return Enumerable.Empty<int>();
            }

            return ids
                .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Select(Int32.Parse);
        }

        public bool AllowSlideTypeChoice {
            get { return this.Retrieve<bool>("AllowSlideTypeChoice", versioned: true); }
            set { this.Store("AllowSlideTypeChoice", value, versioned: true); }
        }

        public string DefaultSlideType {
            get { return this.Retrieve<string>("DefaultSlideType", versioned: true); }
            set { this.Store("DefaultSlideType", value, versioned: true); }
        }
    }
}
