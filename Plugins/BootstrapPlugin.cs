﻿using System;
using System.Globalization;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;

namespace Onestop.SlideShows.Plugins {
    public class BootstrapPlugin : ISlideShowPlugin {
        public BootstrapPlugin(IShapeFactory shapeFactory) {
            Shape = shapeFactory;
        }

        dynamic Shape { get; set; }

        public string Name { get { return "Bootstrap"; } }

        public int Order { get { return 200; } }

        public dynamic GetDisplayShape(ISlideShow slideShow)
        {
            var shape = Shape.BootstrapSlideShowPlugin();
            if (!String.IsNullOrEmpty(slideShow.Get("DisplayAs")))
                shape.Metadata.Alternates.Add("BootstrapSlideShowPlugin_" + slideShow.Get("DisplayAs"));

            return SetupConfigurationShape(shape, slideShow);
        }

        public dynamic GetConfigurationShape(ISlideShow slideShow) {
            return SetupConfigurationShape(Shape.BootstrapSlideShowPlugin_Configuration(), slideShow);
        }

        public void UpdateConfiguration(string prefix, IUpdateModel updater, ISlideShow slideShow) {
            var updatedData = new UpdateViewModel();
            if (updater.TryUpdateModel(updatedData, prefix + ".BootstrapPlugin", null, null)) {
                slideShow
                    .Set("AutoStart", updatedData.AutoStart.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Interval", updatedData.Interval.ToString(CultureInfo.InvariantCulture))
                    .Set("Pause", updatedData.Pause)
                    .Set("Wrap", updatedData.Wrap.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Keyboard", updatedData.Keyboard.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Indicators", updatedData.Indicators.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Controls", updatedData.Controls.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Swipe", updatedData.Swipe.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("DisplayAs", updatedData.DisplayAs);
            }
        }

        private dynamic SetupConfigurationShape(dynamic shape, ISlideShow slideShow)
        {
            shape.AutoStart = slideShow.Get("AutoStart", "true").ToLowerInvariant();
            shape.Interval = slideShow.Get("Interval", "5000");
            shape.Pause = slideShow.Get("Pause", "hover");
            shape.Wrap = slideShow.Get("Wrap", "true").ToLowerInvariant();
            shape.Keyboard = slideShow.Get("Keyboard", "true").ToLowerInvariant();
            shape.Indicators = slideShow.Get("Indicators", "true").ToLowerInvariant();
            shape.Controls = slideShow.Get("Controls", "true").ToLowerInvariant();
            shape.Swipe = slideShow.Get("Swipe", "true").ToLowerInvariant();
            shape.DisplayAs = slideShow.Get("DisplayAs");
            return shape;
        }

        private class UpdateViewModel
        {
            public bool AutoStart { get; set; }
            public int Interval { get; set; }
            public string Pause { get; set; }
            public bool Wrap { get; set; }
            public bool Keyboard { get; set; }
            public bool Indicators { get; set; }
            public bool Controls { get; set; }
            public bool Swipe { get; set; }
            public string DisplayAs { get; set; }
        }
    }
}