﻿using System.Collections.Generic;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Fields;
using Onestop.SlideShows.Models;
using Orchard.ContentManagement;

namespace Onestop.SlideShows.ViewModels {
    public class SlideShowViewModel {
        public IContent ContentPart { get; set; }
        public IEnumerable<SlidePart> Slides { get; set; }
        public IEnumerable<dynamic> SlideShapes { get; set; }
        public IEnumerable<LayoutTemplatePart> Layouts { get; set; }
        public SlideShowField Field { get; set; }
        public string SlideIds { get; set; }
        public IDictionary<string, dynamic> PluginConfigurationEditors { get; set; }
        public string Plugin { get; set; }
        public SlideShowPart SlideShow { get; set; }
        public string SlideOrder { get; set; }
        public bool AllowPluginOverride { get; set; }
        public bool AllowPluginConfigurationOverride { get; set; }
        public bool CanAddSlide { get; set; }
        public bool AllowSlideTypeConfiguration { get; set; }
        public bool AllowSlideTypeChoice { get; set; }
        public string DefaultSlideType { get; set; }
        public IDictionary<string, string> SlideContentTypes { get; set; }
    }
}
