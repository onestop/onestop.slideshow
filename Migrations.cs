﻿using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;

namespace Onestop.SlideShows {
    [OrchardFeature("Onestop.SlideShows")]
    public class Migrations : DataMigrationImpl {
        public int Create() {

        //    SchemaBuilder.CreateTable("SlideShowPartRecord", table => table.ContentPartRecord());

            ContentDefinitionManager.AlterPartDefinition("SlidePart", part => part
                .Attachable()
                .WithDescription("Turns your content type into a slide."));

            ContentDefinitionManager.AlterPartDefinition("SlideShowPart", part => part
                .WithDescription("Turns your content type into a slide show."));

            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
                .WithPart("SlidePart")
                .WithPart("TemplatedItemPart")
                .WithPart("CommonPart"));

            ContentDefinitionManager.AlterTypeDefinition("SlideShow", type => type
                .DisplayedAs("Slide Show")
                .WithPart("SlideShowPart", p => p
                    .WithSetting("SlideShowSettings.Plugin", "Default"))
                .WithPart("CommonPart")
                .WithPart("TitlePart")
                .WithPart("AutoroutePart", builder => builder
                    .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                    .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                    .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name:'Title', Pattern: '{Content.Slug}', Description: 'my-slideshow'}]")
                    .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                .Draftable()
                .Creatable());

            ContentDefinitionManager.AlterPartDefinition("SlideShowWidget", part => part
                .WithField("SlideShow", builder => builder
                    .WithDisplayName("Slide Show")
                    .OfType("ContentPickerField")
                    .WithSetting("ContentPickerFieldSettings.Required", "true")
                    .WithSetting("ContentPickerFieldSettings.Multiple", "false")));

            ContentDefinitionManager.AlterTypeDefinition("SlideShowWidget", type => type
                .DisplayedAs("Slide Show Widget")
                .WithSetting("Stereotype", "Widget")
                .WithPart("WidgetPart")
                .WithPart("SlideShowWidget")
                .WithPart("CommonPart", builder => builder
                    .WithSetting("DateEditorSettings.ShowDateEditor", "false")
                    .WithSetting("OwnerEditorSettings.ShowOwnerEditor", "true"))
                .WithPart("IdentityPart")
                .Creatable(false)
                .Draftable(false));

            return 1;
        }

        public int UpdateFrom1()
        {

            //add parts for export import:

            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
                .WithPart("IdentityPart"));

            ContentDefinitionManager.AlterTypeDefinition("SlideShow", type => type
               .WithPart("IdentityPart"));

            return 2;
        }

        public int UpdateFrom2()
        {
            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
                 .WithSetting("Draftable","false"));

            ContentDefinitionManager.AlterTypeDefinition("SlideShow", type => type
               .WithSetting("Draftable","false"));
            return 3;
        }

        public int UpdateFrom3()
        {//correct bad migrations in 3
            ContentDefinitionManager.AlterTypeDefinition("SlideShow", type => type
               .Draftable(false)
               .Listable(true));
            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
          .Draftable(false)
          .Listable(false));
            return 4;
        }

        public int UpdateFrom4()
        {
            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
                .WithPart("TemplatedItemPart", builder => builder
                    .WithSetting("TemplatedItemPartSettings.AllowTemplateChoice", "True")));

            return 5;
        }
    }
}