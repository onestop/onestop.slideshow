﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Onestop.Layouts.Models;
using Orchard.ContentManagement;
using Orchard.Core.Title.Models;
using Orchard.DisplayManagement;
using Orchard.DisplayManagement.Descriptors;
using Orchard.DisplayManagement.Shapes;
using Orchard.Environment;
using Orchard.Localization;
using Orchard.UI.Resources;

// ReSharper disable InconsistentNaming

namespace Onestop.SlideShows.Shapes {
    public class EditorShapes : IShapeTableProvider {
        private readonly ITagBuilderFactory _tagBuilderFactory;
        private readonly Work<IResourceManager> _resourceManager;

        public Localizer T { get; set; }

        public EditorShapes(ITagBuilderFactory tagBuilderFactory, Work<IResourceManager> resourceManager) {
            _tagBuilderFactory = tagBuilderFactory;
            _resourceManager = resourceManager;
            T = NullLocalizer.Instance;
        }

        public void Discover(ShapeTableBuilder builder) {
            builder.Describe("TemplateListSelector").Configure(descriptor => descriptor.Wrappers.Add("InputWrapper"));
        }

        [Shape]
        public void TemplateListSelector(
            TextWriter Output,
            dynamic Shape,
            IEnumerable<LayoutTemplatePart> Templates, 
            string Name,
            string Value) {

            Shape.Classes.Add("template-ids");
            var hiddenInput = _tagBuilderFactory.Create(Shape, "input");
            hiddenInput.MergeAttribute("type", "hidden");
            if (Name != null) {
                hiddenInput.MergeAttribute("name", Name, false);
            }
            if (Value != null) {
                hiddenInput.MergeAttribute("value", Value, false);
            }
            Output.WriteLine(hiddenInput.ToString(TagRenderMode.SelfClosing));

            var templatesToUseList = new OrchardTagBuilder("ol");
            templatesToUseList.MergeAttribute("style", "list-style-type: decimal");
            templatesToUseList.AddCssClass("template-to-use-list");
            templatesToUseList.MergeAttribute("data-templates-field", Shape.Id);
            Output.WriteLine(templatesToUseList.ToString(TagRenderMode.Normal));

            var templateSelect = new OrchardTagBuilder("select");
            templateSelect.AddCssClass("template-list");
            templateSelect.MergeAttribute("data-templates-field", Shape.Id);
            templateSelect.MergeAttribute("data-templates-delete-text", T("Remove").Text);

            Output.WriteLine(templateSelect.ToString(TagRenderMode.StartTag));

            if (Templates != null) {
                foreach (var template in Templates
                    .Select(t => new {Value = t.Id, Text = t.As<TitlePart>().Title})
                    .OrderBy(t => t.Text)) {

                    var selectItem = new OrchardTagBuilder("option");
                    selectItem.MergeAttribute("value", template.Value.ToString(CultureInfo.InvariantCulture));
                    selectItem.InnerHtml = template.Text;

                    Output.WriteLine(selectItem.ToString(TagRenderMode.Normal));
                }
            }

            Output.WriteLine(templateSelect.ToString(TagRenderMode.EndTag));

            var addButton = new OrchardTagBuilder("button") {
                InnerHtml = T("Add").Text
            };
            addButton.AddCssClass("template-add-button");
            addButton.MergeAttribute("data-templates-field", Shape.Id);
            Output.WriteLine(addButton.ToString(TagRenderMode.Normal));

            _resourceManager.Value.Include(
                "script", "~/Modules/Onestop.SlideShows/Scripts/slideshow-projection-layout.js", "~/Modules/Onestop.SlideShows/Scripts/slideshow-projection-layout.min.js");
        }
    }
}
