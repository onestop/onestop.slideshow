﻿using System;
using Onestop.Layouts.Services;
using Orchard.DisplayManagement;
using Orchard.Forms.Services;
using Orchard.Localization;

namespace Onestop.SlideShows.ProjectionLayouts {
    public class SlideShowLayoutForms : IFormProvider {
        private readonly ITemplateService _templateService;

        protected dynamic Shape { get; set; }
        public Localizer T { get; set; }

        public SlideShowLayoutForms(
            ITemplateService templateService,
            IShapeFactory shapeFactory) {

            _templateService = templateService;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        public void Describe(DescribeContext context) {
            Func<IShapeFactory, object> form =
                shape => {

                    var f = Shape.Form(
                        Id: "SlideShowLayout",
                        _Templates: Shape.Fieldset(
                            Title: T("Templates"),
                            _Templates: Shape.TemplateListSelector(
                                Id: "templates", Name: "Templates",
                                Title: T("Slide Templates"),
                                Description:
                                    T("The list of templates to use in order to render the projection results as a slide show."),
                                Templates: _templateService.GetTemplates()
                                )
                            ),
                        _Dimensions: Shape.Fieldset(
                            Classes: new[] {"expando"},
                            Title: T("Dimensions"),
                            _Width: Shape.TextBox(
                                Id: "slideshow-width", Name: "Width",
                                Title: T("Width"),
                                Description: T("The width of the slide show."),
                                Classes: new[] {"textMedium", "tokenized"}
                                ),
                            _Height: Shape.TextBox(
                                Id: "slideshow-height", Name: "Height",
                                Title: T("Height"),
                                Description: T("The height of the slide show."),
                                Classes: new[] {"textMedium", "tokenized"}
                                )
                            )
                        );

                    return f;
                };

            context.Form("SlideShowLayout", form);

        }
    }
}