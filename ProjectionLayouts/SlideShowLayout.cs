﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Onestop.Layouts.Drivers;
using Onestop.Layouts.Helpers;
using Onestop.Layouts.Models;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Projections.Descriptors.Layout;
using Orchard.Projections.Services;

namespace Onestop.SlideShows.ProjectionLayouts {
    public class SlideShowLayout : ILayoutProvider {
        private readonly IContentManager _contentManager;
        private readonly ITemplatedItemPartDriver _templatedItemPartDriver;
        protected dynamic Shape { get; set; }

        public SlideShowLayout(
            IShapeFactory shapeFactory,
            IContentManager contentManager,
            ITemplatedItemPartDriver templatedItemPartDriver) {
            _contentManager = contentManager;
            _templatedItemPartDriver = templatedItemPartDriver;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public void Describe(DescribeLayoutContext describe) {
            describe.For("Html", T("Html"),T("Html Layouts"))
                .Element("SlideShow", T("Slide Show"), T("Organizes content items as a dynamic slide show."),
                    DisplayLayout,
                    RenderLayout,
                    "SlideShowLayout"
                );
        }

        public LocalizedString DisplayLayout(LayoutContext context) {
            return T("Slide Show");
        }

        public dynamic RenderLayout(LayoutContext context, IEnumerable<LayoutComponentResult> layoutComponentResults) {
            var templateIdData = context.State.Templates;
            if (templateIdData == null) return null;
            var templateIds = ((string)templateIdData)
                .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse);
            var widthString = ((string) context.State.Width);
            var heightString = ((string) context.State.Height);
            int width, height;
            int.TryParse(widthString, out width);
            int.TryParse(heightString, out height);
            var templates = _contentManager.GetMany<LayoutTemplatePart>(templateIds, VersionOptions.Published, QueryHints.Empty).ToList();
            var resultsEnumerator = layoutComponentResults.GetEnumerator();
            resultsEnumerator.MoveNext();
            var slideShapes = templates
                .Select(t => {
                    var slideData = new XElement("data");
                    BuildSlideData(
                         t.LayoutDescriptionDocument.Root,
                         slideData, resultsEnumerator);
                    var templatedItemPart = new TemplatedItemPart();
                    templatedItemPart.Data = slideData.ToString(SaveOptions.None);
                    templatedItemPart.LayoutId = t.Id;
                    return _templatedItemPartDriver.GetDisplayShape(templatedItemPart, "Detail", Shape);
                });

            var slideShowShape = Shape.Parts_SlideShow(
                Id: "slide-show-layout" + context.LayoutRecord.QueryPartRecord.ContentItemRecord.Id,
                Width: width,
                Height: height,
                Slides: slideShapes);
            return slideShowShape;
        }

        public bool BuildSlideData(
            XElement definitions, XElement parentData, IEnumerator<LayoutComponentResult> resultEnumerator) {

            foreach (var definition in definitions.Elements()) {
                var name = definition.Name.ToString();
                var el = new XElement(name);
                parentData.Add(el);
                var hasContext = definition.AttrBool("hascontext");
                if (hasContext) {
                    var current = resultEnumerator.Current;
                    if (current == null) return false;
                    var context = current.ContentItem;
                    el.SetAttributeValue("context", context.Id);
                    if (!resultEnumerator.MoveNext()) return false;
                }
                if (!BuildSlideData(definition, el, resultEnumerator)) return false;
            }
            return true;
        }
    }
}