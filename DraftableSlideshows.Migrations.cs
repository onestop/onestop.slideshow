﻿using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;

namespace Onestop.SlideShows {
    [OrchardFeature("Onestop.SlideShows.Draftable")]
    public class DraftableMigrations : DataMigrationImpl {
        public int Create() {


            ContentDefinitionManager.AlterTypeDefinition("Slide", type => type
                .WithPart("PublishLaterPart")
                .WithPart("ArchiveLater")
                .Draftable(true));

            ContentDefinitionManager.AlterTypeDefinition("SlideShow", type => type
                .WithPart("PublishLaterPart")
                .WithPart("ArchiveLater")
                .Draftable(true));

            return 1;
        }

 
    }
}