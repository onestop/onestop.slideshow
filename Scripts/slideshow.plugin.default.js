(function($) {
    $(function () {

        OS.init("orchard");

        $(".slide-show-default").each(function () {
            var element = $(this);
            var id = element.attr("id");
            var animationSpeed = element.data("animation-speed");
            var animationTime = element.data("animation-time");
            var minWidth = element.data("min-width");
            var auto = element.data("auto");
            var cycleOnce = element.data("cycle-once");
            var cycling = element.data("cycling");
            var buttons = element.data("buttons");
            var indicators = element.data("indicators");
            var orientation = element.data("orientation");
            var stopAtLast = element.data("stop-at-last");

            OS.mod.slideshow.config({
                selector: "#" + id,
                animationSpeed: animationSpeed,
                animationTime: animationTime,
                minWidth: minWidth,
                auto: auto,
                cycleOnce: cycleOnce,
                cycling: cycling,
                buttons: buttons,
                indicators: indicators,
                orientation: orientation,
                stopAtLast: stopAtLast,
                useCustomColor: false,
            });
        });
        
    });
})(jQuery);