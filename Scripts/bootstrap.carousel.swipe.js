﻿(function () {
    $(".carousel.slide").each(function () {
        $carousel = $(this);
        $carousel.swipe({
            preventDefaultEvents: false, // important so anchors are clickable
            swipeLeft: function (event) {
                $carousel.carousel("next");
            },
            swipeRight: function (event) {
                $carousel.carousel("prev");
            },
            excludedElements: "label, button, input, select, textarea, .noSwipe",
            threshold: 1
        });
    });
})();
