﻿$(function () {
    var findTemplatesToUseList = function(templateFieldId) {
        return $(".template-to-use-list[data-templates-field=\"" + templateFieldId + "\"]");
    },
        findList = function(templateFieldId) {
            return $(".template-list[data-templates-field=\"" + templateFieldId + "\"]");
        },
        updateHiddenInputFromDropDowns = function() {
            $(".template-ids").each(function() {
                $(this).val(
                    findTemplatesToUseList(this.id)
                        .find("select")
                        .map(function() {
                            return $(this).val();
                        })
                        .get()
                        .join(","));
            });
        },
        appendTemplate = function (templatesToUseList, list, templateId) {
            var newList = list
                .clone()
                .removeClass("template-list")
                .addClass("template-drop-down"),
                newDeleteButton = $("<button></button>")
                    .text(list.data("templates-delete-text"))
                    .addClass("link")
                    .click(function() {
                        $(this).closest("li")
                            .remove();
                        updateHiddenInputFromDropDowns();
                    });
            if (templateId) newList.val(templateId);
            templatesToUseList.append(
                $("<li></li>")
                    .append(newList)
                    .append(newDeleteButton));
        };

    // Initialize list of templates from the list of values in the hidden field
    $(".template-ids").each(function() {
        var templateField = $(this),
            templateFieldId = templateField.attr("id"),
            list = findList(templateFieldId),
            templatesToUseList = findTemplatesToUseList(templateFieldId),
            templateIds = templateField.val().split(",");
        $.each(templateIds, function(index, value) {
            var templateId = $.trim(value);
            if (!templateId) return;
            appendTemplate(templatesToUseList, list, templateId);
        });
    });
    // Update the hidden field on any change of any of the template drop-downs
    $(".template-drop-down").change(function () {
        updateHiddenInputFromDropDowns();
    });
    // Add a new template to the list
    $(".template-add-button")
        .click(function (e) {
            e.preventDefault();
            var button = $(this),
                templateFieldId = button.data("templates-field"),
                templatesToUseList = findTemplatesToUseList(templateFieldId),
                list = findList(templateFieldId);
            appendTemplate(templatesToUseList, list, list.val());
            updateHiddenInputFromDropDowns();
        });
});