﻿(function ($) {
    // Make lists of slides sortable
    if ($.fn.sortable) {
        $(".slideshow-slides").sortable({
            handle: ".related",
            stop: function(event) {
                var sortable = $(event.target).closest(".slideshow-slides");
                $("#" + sortable.data("order-field"))
                    .val(sortable.find(".slide-row")
                        .map(function() {
                            return $(this).data("slide-id");
                        })
                        .get().join(","));

                sortable.siblings('.save-message-slideshow').show();
            }
        }).disableSelection();
    }

    // Prepare a data structure on the plugn drop-downs to easily find each plugin configuration element
    $(".slideshow-plugin-configuration").each(function () {
        var pluginConfigurationElement = $(this),
            pluginDropDown = $("#" + pluginConfigurationElement.data("slideshow-plugin-dropdown")),
            pluginConfigurationElements = pluginDropDown.data("config-elements");
        if (!pluginConfigurationElements) {
            pluginDropDown.data("config-elements", pluginConfigurationElements = {});
        }
        pluginConfigurationElements[pluginConfigurationElement.data("slideshow-plugin")] = pluginConfigurationElement;
    });
    
    // Handle plugin changes
    $(".slideshow-plugin-dropdown")
        .on("change", function () {
            var dropdown = $(this),
                val = dropdown.val();
            $.each(dropdown.data("config-elements"), function(plugin, configurationElement) {
                configurationElement.toggle(plugin == val);
            });
        });
})(jQuery);