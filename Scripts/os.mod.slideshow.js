window.OS.mod.slideshow = (function () {
    //Public Variables
    var version = '1.0.0';
    var animationSpeed = 1000;
    var animationTime = 5000;
    var buttons = true;
    var indicators = false;
    var auto = false;
    var cycleOnce = false;
    var cycling = true;
    var orientation = 'horizontal';
    var minWidth = 768;
    var stopAtLast = false;
    var useCustomColor = false;
    var arrowColorDefault = '#fff';
    var arrowColorHover = '#333';

    var indicatorsDefault = '#666';
    var indicatorsActive = '#fff';
    var indicatorsHover = '#ccc';


    //Private Variables
    var slides = null;
    var currentSlide = [];
    var counterSlide = [];
    var slideObj = [];
    var indicatorObj = [];
    var numOfSlides = [];
    var nextSlide = [];
    var selector = '';
    var timer = [];
    var buttonActive = [];
    var slideshowIndex = 0;
    var slideDirection = [];
    var slideOrientation = [];
    var touchDevice = "ontouchstart" in document;
    return {
        init: function () {
            OS.log("OS.mod.slideshow has been initialized");
            OS.mod.slideshow.resize();
        },
        config: function (settingsObj) {
            OS.log("OS.mod.slideshow.config has been initialized");
            if (settingsObj.selector) {
                selector = settingsObj.selector;
                $(selector).data("selector", settingsObj.selector);
            }
            if (settingsObj.animationSpeed) {
                $(selector).data("animationSpeed", settingsObj.animationSpeed);
            } else {
                $(selector).data("animationSpeed", animationSpeed);
            }
            if (settingsObj.animationTime) {
                $(selector).data("animationTime", settingsObj.animationTime);
            } else {
                $(selector).data("animationTime", animationTime);
            }
            if (settingsObj.auto != null) {
                $(selector).data("auto", settingsObj.auto);
            } else {
                $(selector).data("auto", auto);
            }
            if (settingsObj.minWidth) {
                $(selector).data("minWidth", settingsObj.minWidth);
            } else {
                $(selector).data("minWidth", minWidth);
            }
            if (settingsObj.buttons != null) {
                $(selector).data("buttons", settingsObj.buttons);
            } else {
                $(selector).data("buttons", buttons);
            }
            if (settingsObj.indicators != null) {
                $(selector).data("indicators", settingsObj.indicators);
            } else {
                $(selector).data("indicators", indicators);
            }
            if (settingsObj.cycleOnce != null) {
                $(selector).data("cycleOnce", settingsObj.cycleOnce);
            } else {
                $(selector).data("cycleOnce", cycleOnce);
            }
            if (settingsObj.orientation != null) {
                $(selector).data("orientation", settingsObj.orientation);
            } else {
                $(selector).data("orientation", orientation);
            }
            if (settingsObj.stopAtLast != null) {
                $(selector).data("stopAtLast", settingsObj.stopAtLast);
            } else {
                $(selector).data("stopAtLast", stopAtLast);
            }
            if (settingsObj.cycling != null) {
                $(selector).data("cycling", settingsObj.cycling);
            } else {
                $(selector).data("cycling", cycling);
            }
            if (settingsObj.useCustomColor) {
                $(selector).data("useCustomColor", settingsObj.useCustomColor);
            } else {
                $(selector).data("useCustomColor", useCustomColor);
            }
            if (settingsObj.arrowColorDefault) {
                $(selector).data("arrowColorDefault", settingsObj.arrowColorDefault);
            } else {
                $(selector).data("arrowColorDefault", arrowColorDefault);
            }
            if (settingsObj.arrowColorHover) {
                $(selector).data("arrowColorHover", settingsObj.arrowColorHover);
            } else {
                $(selector).data("arrowColorHover", arrowColorHover);
            }
            if (settingsObj.indicatorsDefault) {
                $(selector).data("indicatorsDefault", settingsObj.indicatorsDefault);
            } else {
                $(selector).data("indicatorsDefault", indicatorsDefault);
            }
            if (settingsObj.indicatorsActive) {
                $(selector).data("indicatorsActive", settingsObj.indicatorsActive);
            } else {
                $(selector).data("indicatorsActive", indicatorsActive);
            }
            if (settingsObj.indicatorsHover) {
                $(selector).data("indicatorsHover", settingsObj.indicatorsHover);
            } else {
                $(selector).data("indicatorsHover", indicatorsHover);
            }



            $(selector).data("index", slideshowIndex);
            slideshowIndex = slideshowIndex + 1;
            $(selector).data("currentSlide", 0);

            if (slideshowIndex === $(".os-slideshow").length) {
                OS.mod.slideshow.run();
            }
        },
        run: function () {
            $(".os-slideshow").each(function (index) {
                var slideshowId = $(this).attr("id");
                OS.mod.slideshow.setControls(slideshowId);
                OS.mod.slideshow.setMinWidth(slideshowId);
                OS.mod.slideshow.setZIndex(slideshowId);
                OS.mod.slideshow.autoRun(slideshowId, index);
            });
            OS.mod.slideshow.setEvents();
            OS.resizeHandler("OS.mod.slideshow.resize");
        },
        getSelector: function () {
            return selector;
        },
        autoRun: function (id, index) {
            if ($("#" + id).data("stopAtLast") == true) {
                if (currentSlide[index] == (numOfSlides[index] - 1)) {
                    clearTimeout(timer[index]);
                    return;
                }
            }
            else if ($("#" + id).data("cycleOnce") == true) {
                if (currentSlide[index] == 0) {
                    clearTimeout(timer[index]);
                    return;
                }
            }
            if ($("#" + id).data("auto") == true) {
                clearTimeout(timer[index]);
                timer[index] = setTimeout(function () {
                    OS.mod.slideshow.cycle(id, 'next');
                    OS.mod.slideshow.autoRun(id, index);
                }, $("#" + id).data("animationTime"));
            }
        },
        resize: function () {
            $(".os-slideshow").each(function (index) {
                clearTimeout(timer[index]);
                if ($("#" + $(this).attr("id")).data("auto") === true) {
                    OS.mod.slideshow.autoRun($(this).attr("id"), index);
                }
                $("#" + $(this).attr("id")).width($(this).parent().width());
                $("#" + $(this).attr("id") + " ul li").width($(this).parent().width());
                slideshowHeight = $("#" + $(this).attr("id") + " ul li:visible img").height();
                if (slideshowHeight > 0) {
                    $("#" + $(this).attr("id")).height(slideshowHeight);
                    $("#" + $(this).attr("id") + " ul li").height(slideshowHeight);
                    $("#" + $(this).attr("id") + " ul li").height($("#" + $(this).attr("id")).height());
                }
                else {
                    $(".os-slideshow ul li").css({ "position": "relative" });
                }
            });
        },
        setControls: function (id) {
            var indicatorString = '';
            if ($("#" + id).data("buttons") == true) {
                $("#" + id).append('<span class="prev" data-id="' + id + '"><div class="arrow-left"></div></span><span class="next" data-id="' + id + '"><div class="arrow-right"></div></span>')

                if ($("#" + id).data("cycling") == false) {
                    $(".prev").hide();
                }
            }

            if ($("#" + id).data("indicators") == true) {
                indicatorString += '<ol>';
                $("#" + id + " ul li").each(function (index) {
                    if (index == 0) {
                        indicatorString += '<li class="active" data-id="' + id + '" data-index="' + index + '">' + (index + 1) + '</li>';
                    }
                    else {
                        indicatorString += '<li data-id="' + id + '" data-index="' + index + '">' + (index + 1) + '</li>';
                    }
                });
                indicatorString += '</ol>';
                $("#" + id).append(indicatorString);
            }

            if ($("#" + id).data("useCustomColor") == true) {
                $(".arrow-left").css({ "border-color": "transparent " + $("#" + id).data("arrowColorDefault") + " transparent transparent" });
                $(".arrow-right").css({ "border-color": "transparent transparent transparent " + $("#" + id).data("arrowColorDefault") });

                $(".arrow-left")
                .on("mouseenter", function () {
                    $(this).css({ "border-color": "transparent " + $("#" + id).data("arrowColorHover") + " transparent transparent" });
                })
                .on("mouseleave", function () {
                    $(this).css({ "border-color": "transparent " + $("#" + id).data("arrowColorDefault") + " transparent transparent" });
                });
                $(".arrow-right")
                .on("mouseenter", function () {
                    $(this).css({ "border-color": "transparent transparent transparent " + $("#" + id).data("arrowColorHover") });
                })
                .on("mouseleave", function () {
                    $(this).css({ "border-color": "transparent transparent transparent " + $("#" + id).data("arrowColorDefault") });
                });

                $(".os-slideshow ol li").css({ "background": $("#" + id).data("indicatorsDefault") });
                $(".os-slideshow ol li.active").css({ "background": $("#" + id).data("indicatorsActive") });

                $(".os-slideshow ol li")
                .on("mouseenter", function () {
                    if (!$(this).hasClass("active")) {
                        $(this).css({ "background": $("#" + id).data("indicatorsHover") });
                    }
                })
                .on("mouseleave", function () {
                    if (!$(this).hasClass("active")) {
                        $(this).css({ "background": $("#" + id).data("indicatorsDefault") });
                    }
                });
            }
        },
        setZIndex: function (id) {
            numOfSlide = $("#" + id + " li").length;
            $("#" + id + " li").each(function (index) {
                $(this).css({ "z-index": (numOfSlide - index) });
            });
        },
        setMinWidth: function (id) {
            if ($("#" + id).data("minWidth") > 0) {
                $("#" + id + " ul li").css({ "min-width": $("#" + id).data("minWidth") + "px" });
                $("#" + id).css({ "min-width": $("#" + id).data("minWidth") + "px" });
            }
        },
        setEvents: function () {
            if (touchDevice) {
                eventClick = "touchstart";
                eventMove = "touchmove";
            }
            else {
                eventClick = "click";
                eventMove = "mousemove";
            }

            $(".os-slideshow")
            .on(eventClick, ".next", function () {
                index = $("#" + $(this).attr("data-id")).data("index");
                OS.mod.slideshow.next($(this).attr("data-id"), index);
            })
            .on(eventClick, ".prev", function () {
                index = $("#" + $(this).attr("data-id")).data("index");
                OS.mod.slideshow.prev($(this).attr("data-id"), index);
            })
            .on(eventClick, "ol>li[class!='active']", function () {
                index = $(this).index();
                OS.mod.slideshow.show($(this).attr("data-id"), index);
            });
            if (touchDevice) {
                $(".os-slideshow")
                .swipe({
                    preventDefaultEvents: false,
                    swipe: function (event, direction, distance, duration, fingerCount) {
                        if (direction == "left") {
                            $(".next").trigger(eventClick);
                        }
                        if (direction == "right") {
                            $(".prev").trigger(eventClick);
                        }
                    },
                    allowPageScroll: "vertical",
                    threshold: 40
                });
            }
        },
        next: function (id, index) {
            if (!OS.isValidObject(id)) {
                return;
            }
            if (buttonActive[index] == true || typeof (buttonActive[index]) == "undefined") {
                buttonActive[index] = false;
                clearTimeout(timer[index]);
                $("#" + id + " ul li").css({ "top": 0 });
                $("#" + id).data("auto", false);
                OS.mod.slideshow.cycle(id, 'next');
            }
        },
        prev: function (id, index) {
            if (!OS.isValidObject(id)) {
                return;
            }
            if (buttonActive[index] == true || typeof (buttonActive[index]) == "undefined") {
                buttonActive[index] = false;
                clearTimeout(timer[index]);
                $("#" + id + " ul li").css({ "top": 0 });
                $("#" + id).data("auto", false);
                OS.mod.slideshow.cycle(id, 'prev');
            }
        },
        show: function (id, slideIndex) {
            if (!OS.isValidObject(id)) {
                return;
            }
            var index = $("#" + id).data("index");
            if (buttonActive[index] == true || typeof (buttonActive[index]) == "undefined") {
                buttonActive[index] = false;
                clearTimeout(timer[index]);
                $("#" + id + " ul li").css({ "left": 0 });
                $("#" + id).data("auto", false);
                if ($("#" + id).data("currentSlide") < slideIndex) {
                    OS.mod.slideshow.cycle(id, 'next', slideIndex);
                }
                else {
                    OS.mod.slideshow.cycle(id, 'prev', slideIndex);
                }
            }
        },
        cycle: function (id, action, getSlide) {
            var index = $("#" + id).data("index");
            slideOrientation[index] = $("#" + id).data("orientation");
            slideObj[index] = $("#" + id + " ul li");
            indicatorObj[index] = $("#" + id + " ol li");
            numOfSlides[index] = slideObj[index].length;
            currentSlide[index] = $("#" + id).data("currentSlide");
            if (!OS.isValidObject(counterSlide[index])) {
                counterSlide[index] = 0;
            }
            OS.mod.slideshow.resize();
            slideObj[index].css({ "position": "absolute" });

            var slidePosIni = "100%";
            var slidePosEnd = "-100%";
            if (action === 'prev') {
                slidePosIni = "-100%";
                slidePosEnd = "100%";
            }
            if (!OS.isValidObject(getSlide)) {
                if (action === 'prev') {
                    if (counterSlide[index] == 0) {
                        counterSlide[index] = numOfSlides[index];
                    }
                    counterSlide[index] = counterSlide[index] - 1;
                }
                else {
                    counterSlide[index] = counterSlide[index] + 1;
                }
                nextSlide[index] = Math.abs(parseInt(counterSlide[index]) % parseInt(numOfSlides[index]));
            } else {
                counterSlide[index] = getSlide;
                nextSlide[index] = getSlide;
            }
            indicatorObj[index].removeClass("active");
            indicatorObj[index].eq(nextSlide[index]).addClass("active");

            if ($("#" + id).data("useCustomColor") == true) {
                $(".os-slideshow ol li").css({ "background": $("#" + id).data("indicatorsDefault") });
                $(".os-slideshow ol li.active").css({ "background": $("#" + id).data("indicatorsActive") });
            }

            switch (slideOrientation[index]) {
                case 'horizontal':
                    slideObj[index].css({ "left": 0 });
                    slideObj[index].eq(nextSlide[index]).css({ "left": slidePosIni });
                    slideObj[index].eq(nextSlide[index]).show();
                    slideObj[index].eq(currentSlide[index]).animate({ left: slidePosEnd }, $("#" + id).data("animationSpeed"), function () {
                        $(this).hide().css({ "left": 0 });
                        OS.mod.slideshow.resize();
                        buttonActive[index] = true;
                    });
                    slideObj[index].eq(nextSlide[index]).animate({ left: 0 }, $("#" + id).data("animationSpeed"));
                    break;
                case 'vertical':
                    slideObj[index].css({ "top": 0 });
                    slideObj[index].eq(nextSlide[index]).css({ "top": slidePosIni });
                    slideObj[index].eq(nextSlide[index]).show();
                    slideObj[index].eq(currentSlide[index]).animate({ top: slidePosEnd }, $("#" + id).data("animationSpeed"), function () {
                        $(this).hide().css({ "top": 0 });
                        OS.mod.slideshow.resize();
                        buttonActive[index] = true;
                    });
                    slideObj[index].eq(nextSlide[index]).animate({ top: 0 }, $("#" + id).data("animationSpeed"));
                    break;
                case 'fading':
                    slideObj[index].eq(nextSlide[index]).fadeIn($("#" + id).data("animationSpeed"))
                    slideObj[index].eq(currentSlide[index]).fadeOut($("#" + id).data("animationSpeed"));
                    buttonActive[index] = true;
                    break;
                default:
            }

            currentSlide[index] = nextSlide[index];
            $("#" + id).data("currentSlide", currentSlide[index]);

            if ($("#" + id).data("buttons") == true && $("#" + id).data("cycling") == false) {
                $(".next").show();
                if ((currentSlide[index] + 1) == numOfSlides[index]) {
                    $(".next").hide();
                }
                $(".prev").hide();
                if (currentSlide[index] > 0) {
                    $(".prev").show();
                }
            }
        },
        pause: function (id) {
            var index = $("#" + id).data("index");
            $("#" + id).data("auto", false);
            clearTimeout(timer[index]);
        },
        play: function(id) {
            var index = $("#" + id).data("index");
            $("#" + id).data("auto", true);
            OS.mod.slideshow.autoRun(id, index);
        },
        getVersion: function () {
            return version;
        }
    }
} ());