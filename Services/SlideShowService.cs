﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Fields;
using Onestop.SlideShows.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.Core.Common.Models;
using Orchard.Core.Contents.ViewModels;
using Orchard.Core.Title.Models;
using Orchard.Environment.Extensions;
using Orchard.UI.Navigation;

namespace Onestop.SlideShows.Services {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlideShowService : ISlideShowService {
        private readonly IContentManager _contentManager;
        private readonly IEnumerable<ISlideShowPlugin> _plugins;
        private readonly IVersionManager _versionManager;

        public SlideShowService(IContentManager contentManager, IEnumerable<ISlideShowPlugin> plugins, IVersionManager versionManager) {
            _contentManager = contentManager;
            _plugins = plugins;
            _versionManager = versionManager;
        }

        public IEnumerable<SlideShowPart> GetSlideShows(out int count, Pager pager, ContentsOrder orderBy = ContentsOrder.Published) {
            var query = _contentManager.Query<SlideShowPart>(VersionOptions.Latest).Join<CommonPartRecord>();
            switch (orderBy) {
                case ContentsOrder.Modified:
                    query.OrderByDescending(cr => cr.ModifiedUtc);
                    break;
                case ContentsOrder.Published:
                    query.OrderByDescending(cr => cr.PublishedUtc);
                    break;
                case ContentsOrder.Created:
                    query.OrderByDescending(cr => cr.CreatedUtc);
                    break;
            }
            count = query.Count();
            return query.Slice(pager.GetStartIndex(), pager.PageSize).Select(x => x.As<SlideShowPart>()).ToList();
        }

        public IEnumerable<SlidePart> GetSlidesFor(SlideShowPart slideShow, VersionOptions versionOptions = null) {
            return GetSlides(slideShow.SlideIds, versionOptions);
        }

        public IEnumerable<SlidePart> GetSlides(IEnumerable<int> slideIds, VersionOptions versionOptions = null) {
            var slides = slideIds as IList<int> ?? slideIds.ToArray();
            if (!slides.Any())
                return Enumerable.Empty<SlidePart>();

            versionOptions = versionOptions ?? VersionOptions.Published;

            if (versionOptions.IsAllVersions) {
                // We actually don't want all versions, but the only the latest versions EVEN if they are deleted.
                return _versionManager.GetLatest<SlidePart>(slides, new QueryHints().ExpandParts<TitlePart, TemplatedItemPart>());
            }
            
            return _contentManager
                .GetMany<SlidePart>(
                    slides,
                    versionOptions,
                    new QueryHints().ExpandParts<TitlePart, TemplatedItemPart>());
        }

        public IEnumerable<LayoutTemplatePart> GetLayoutsFor(IEnumerable<SlidePart> slides) {
            return _contentManager.GetMany<LayoutTemplatePart>(slides
                    .Where(s => s.As<TemplatedItemPart>().LayoutId != null)
                    .Select(s => s.As<TemplatedItemPart>().LayoutId.Value)
                    .Distinct(),
                    VersionOptions.Published,
                    QueryHints.Empty)
                    .ToList();
        }

        public IDictionary<string, dynamic> GetConfigurationEditorsFor(ISlideShow slideShow) {
            return _plugins.ToDictionary(
                p => p.Name,
                p => p.GetConfigurationShape(slideShow));
        }

        public void UpdatePluginConfiguration(ISlideShow slideShow, string prefix, IUpdateModel updater) {
            updater.TryUpdateModel(slideShow, prefix, new[] { "Plugin" }, new[] { "Slides" });
            var plugin = _plugins.FirstOrDefault(p => p.Name == slideShow.Plugin);
            if (plugin != null) {
                plugin.UpdateConfiguration(prefix, updater, slideShow);
            }
        }
        
        public IEnumerable<ContentTypeDefinition> GetSlideTypes() {
            return _contentManager
                .GetContentTypeDefinitions()
                .Where(d => {
                    var parts = d.Parts.Select(p => p.PartDefinition.Name).ToList();
                    return parts.Contains("SlidePart") && parts.Contains("TemplatedItemPart");
                });
        }

        public void AddSlideTo(IContent container, string fieldName, SlidePart slide) {
            AddSlidesTo(container, fieldName, new[] { slide });
        }

        public void AddSlidesTo(IContent container, string fieldName, IEnumerable<SlidePart> slides) {
            var slideShow = GetSlideShow(container, fieldName);
            var newSlides = slides.ToArray();
            var newIds = slideShow.SlideIds.ToList();

            newIds.AddRange(newSlides.Select(x => x.Id));
            slideShow.SlideIds = newIds;

            foreach (var slide in newSlides) {
                slide.ContainerId = container.Id;
            }
        }

        public dynamic GetPluginDisplayShape(ISlideShow slideShow) {
            var plugin = _plugins.FirstOrDefault(p => p.Name == slideShow.Plugin) ?? _plugins.OrderBy(p => p.Order).FirstOrDefault();
            return plugin != null ? plugin.GetDisplayShape(slideShow) : null;
        }

        public IEnumerable<ISlideShowPlugin> GetPlugins() {
            return _plugins.OrderBy(p => p.Order).ToList();
        }

        public ISlideShow GetSlideShow(IContent container, string fieldName) {
            if (container.Has<SlideShowPart>()) {
                return container.As<SlideShowPart>();
            }
            return GetSlideShowField(container, fieldName);
        }

        private static SlideShowField GetSlideShowField(IContent container, string fieldName) {
            return container.ContentItem
                .Parts
                .SelectMany(p => p.Fields)
                .FirstOrDefault(f => f.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase))
                as SlideShowField;
        }
    }
}
