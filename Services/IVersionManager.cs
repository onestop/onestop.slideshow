﻿using System.Collections.Generic;
using Orchard;
using Orchard.ContentManagement;

namespace Onestop.SlideShows.Services {
    /// <summary>
    /// Returns latest versions of content items, regardless of their deletion status.
    /// </summary>
    public interface IVersionManager : IDependency {
        /// <summary>
        /// Returns the latest versions of the specified list of content items.
        /// </summary>
        IEnumerable<ContentItem> GetLatest(IEnumerable<int> contentItemIds, QueryHints hints = null);

        /// <summary>
        /// Returns the latest versions of the specified list of content items.
        /// </summary>
        IEnumerable<T> GetLatest<T>(IEnumerable<int> contentItemIds, QueryHints hints = null) where T : class, IContent;
    }
}