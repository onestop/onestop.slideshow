﻿using System.Collections.Generic;
using Onestop.SlideShows.Models;

namespace Onestop.SlideShows.Services {
    public class SlideComparer : IEqualityComparer<SlidePart> {
        public bool Equals(SlidePart x, SlidePart y) {
            return x.Id == y.Id;
        }

        public int GetHashCode(SlidePart obj) {
            return obj.Id.GetHashCode();
        }
    }
}